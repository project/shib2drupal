Shib2Drupal provides HTTP authorization and mapping between Shibboleth HTTP 
headers and Drupal roles.

INSTALLATION
------------

Like any other Drupal modules, simply unpack "sites/all/modules" and enable it.
Of course you need to edit your Apache configuration to protect your Drupal
installation (warning: cron.php must still be accessible anonymously). 

For example you can use LocationMatch like this:

  <LocationMatch "/[^cron\.php]">
    AuthType shibboleth
    ShibRequireSession On
    Require valid-user
  </LocationMatch>
 

CONFIGURATION
-------------

Currently no UI is provided (work is in progress), you'll have to manually edit
the code to make this module suit your needs. Sorry for that :(


HOW THE MODULE WORKS
--------------------

1) During Drupal bootstrap the module checks if REMOTE_USER has a value (this is
always true when Shibboleth is working on a directory). 

2) If a user with name == REMOTE_USER already exists it is loaded and the user is
logged is with that username. If no matching user is found then a new user is 
created.

3) The module also provides a mapping for roles: you have to make Shibboleth 
populate a variable named "shib-departmentNumber" on your Web server, with a 
semicolon-separated of "drupal::GROUP" groups, for example:

  drupal::role1;drupal::role2

Notice that roles must already exist in Drupal, they are not automatically created.

4) You should replace the standard "logout" link with the logout provided by the module
which perform the logout from Drupal and notices the user that to logout from Shibboleth
he/she must close and re-open the browser.

Maurizio Pinotti <maurizio _at_ pinottinet _dot_com>
